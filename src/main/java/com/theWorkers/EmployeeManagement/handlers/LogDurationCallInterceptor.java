package com.theWorkers.EmployeeManagement.handlers;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Component
public class LogDurationCallInterceptor implements HandlerInterceptor {

	private static Logger log = LoggerFactory.getLogger(LogDurationCallInterceptor.class);
	private Long startTime = 0L;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		logRequest(request);
		return true;
	}

	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		String duration = Long.toString((System.nanoTime() - this.startTime) / 1000000L);
		log.debug("Request processing duration: {}ms", duration);
		log.info("Request processing duration: {}ms", duration);
		log.debug("==========================request end================================================");
	}

	private String getParameters(HttpServletRequest request) {
		StringBuffer posted = new StringBuffer();
		Enumeration<?> e = request.getParameterNames();
		if (e != null && e.hasMoreElements()) {
			posted.append("?");
		}
		while (e.hasMoreElements()) {
			if (posted.length() > 1) {
				posted.append("&");
			}
			String curr = (String) e.nextElement();
			posted.append(curr + "=");
			posted.append(request.getParameter(curr));
		}
		String ip = request.getHeader("X-FORWARDED-FOR");
		return posted.toString();
	}

	private void logRequest(HttpServletRequest request) throws IOException {

		this.startTime = System.nanoTime();
		String parameters = getParameters(request);
		if (log.isDebugEnabled()) {
			ServletInputStream inputStream = request.getInputStream();
			log.info("===========================request begin================================================");
			log.info("URI         : {}", request.getRequestURI());
			log.info("Method      : {}", request.getMethod());
			log.info("Headers     : {}", request.getHeaderNames());
			log.info("Parameters     : {}", parameters);
			log.info("Request body: {}", new String(IOUtils.toByteArray(inputStream), "UTF-8"));
		} else {
			log.info("{} {}{}", request.getMethod().toUpperCase(), request.getRequestURI(), parameters);
		}
	}

	// private void logResponse(ClientHttpResponse response) throws IOException {
	// if (log.isDebugEnabled()) {
	// log.debug("============================response
	// begin==========================================");
	// log.debug("Status code : {}", response.getStatusCode());
	// log.debug("Status text : {}", response.getStatusText());
	// log.debug("Headers : {}", response.getHeaders());
	// log.debug("Response body: {}", StreamUtils.copyToString(response.getBody(),
	// Charset.defaultCharset()));
	// log.debug("=======================response
	// end=================================================");
	// }
	// }
}
// public ClientHttpResponse intercept(HttpRequest req, byte[] reqBody,
// ClientHttpRequestExecution ex)
// throws IOException {
// RestTemplate restTemplate = null;
// if (LOGGER.isDebugEnabled()) {
// ClientHttpRequestFactory factory
// = new BufferingClientHttpRequestFactory(new
// SimpleClientHttpRequestFactory());
// restTemplate = new RestTemplate(factory);
// } else {
// restTemplate = new RestTemplate();
// }
// List<ClientHttpRequestInterceptor> interceptors =
// restTemplate.getInterceptors();
// if (CollectionUtils.isEmpty(interceptors)) {
// interceptors = new ArrayList<>();
// }
// interceptors.add(new LogDurationCallInterceptor());
// restTemplate.setInterceptors(interceptors);
//
// LOGGER.debug("Request body: {}", new String(reqBody,
// StandardCharsets.UTF_8));
// ClientHttpResponse response = ex.execute(req, reqBody);
// InputStreamReader isr = new InputStreamReader(response.getBody(),
// StandardCharsets.UTF_8);
//
// String body = new
// BufferedReader(isr).lines().collect(Collectors.joining("\n"));
// LOGGER.debug("Response body: {}", body);
// return response;
// }
//
// implements HandlerInterceptor {
//
// public boolean preHandle(HttpServletRequest request, HttpServletResponse
// response, Object handler) {
// HttpServletRequest requestCacheWrapperObject = new
// ContentCachingRequestWrapper(request);
// requestCacheWrapperObject.getParameterMap();
// try {
// log.info(IOUtils.toString(requestCacheWrapperObject.getInputStream()));
//
// } catch (IOException e) {
// log.error("couldn't read strem of requestCacheWrapperObject");
// }
// return true;
// }
//
// public void afterCompletion(HttpServletRequest request, HttpServletResponse
// response, Object handler,
// Exception ex) {
// }
// }
