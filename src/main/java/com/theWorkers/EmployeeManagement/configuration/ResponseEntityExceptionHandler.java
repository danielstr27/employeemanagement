package com.theWorkers.EmployeeManagement.configuration;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.theWorkers.EmployeeManagement.errorHandling.EmployeeManagementError;
import com.theWorkers.EmployeeManagement.exceptions.*;

@ControllerAdvice
class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(value = { AddressNotFoundException.class })
	protected ResponseEntity<Object> handleAddressNotFound(RuntimeException exception, WebRequest request) {
		return handleExceptionInternal(exception,
				new EmployeeManagementError(HttpStatus.NOT_FOUND, exception.getMessage()), new HttpHeaders(),
				HttpStatus.NOT_FOUND, request);
	}

	@ExceptionHandler(value = { ChildNotFoundException.class })
	protected ResponseEntity<Object> handleChildNotFound(RuntimeException exception, WebRequest request) {
		return handleExceptionInternal(exception,
				new EmployeeManagementError(HttpStatus.NOT_FOUND, exception.getMessage()), new HttpHeaders(),
				HttpStatus.NOT_FOUND, request);
	}

	@ExceptionHandler(value = { SpouseNotFoundException.class })
	protected ResponseEntity<Object> handleSpouseNotFound(RuntimeException exception, WebRequest request) {
		return handleExceptionInternal(exception,
				new EmployeeManagementError(HttpStatus.NOT_FOUND, exception.getMessage()), new HttpHeaders(),
				HttpStatus.NOT_FOUND, request);
	}

	@ExceptionHandler(value = { EmployeeNotFoundException.class })
	protected ResponseEntity<Object> handleEmployeeNotFound(RuntimeException exception, WebRequest request) {
		return handleExceptionInternal(exception,
				new EmployeeManagementError(HttpStatus.NOT_FOUND, exception.getMessage()), new HttpHeaders(),
				HttpStatus.NOT_FOUND, request);
	}

	@ExceptionHandler(value = { EmployeeManagementException.class })
	protected ResponseEntity<Object> handleGeneralSystemException(RuntimeException exception, WebRequest request) {
		return handleExceptionInternal(exception,
				new EmployeeManagementError(HttpStatus.BAD_REQUEST, exception.getMessage()), new HttpHeaders(),
				HttpStatus.BAD_REQUEST, request);
	}

//	@ExceptionHandler(value = { RuntimeException.class })
//	@ResponseStatus(HttpStatus.NOT_FOUND)
//	protected ResponseEntity<Object> handleNotFound(RuntimeException exception, WebRequest request) {
//		return handleExceptionInternal(exception, new EmployeeManagementError(HttpStatus.NOT_FOUND, "Internal Error"),
//				new HttpHeaders(), HttpStatus.NOT_FOUND, request);
//	}
}