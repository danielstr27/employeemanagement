package com.theWorkers.EmployeeManagement.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.theWorkers.EmployeeManagement.handlers.LogDurationCallInterceptor;

@Configuration
public class LogDurationMvcConfiguration implements WebMvcConfigurer {

	private final LogDurationCallInterceptor logDurationCallInterceptor;

	public LogDurationMvcConfiguration(LogDurationCallInterceptor logDurationCallInterceptor) {
		this.logDurationCallInterceptor = logDurationCallInterceptor;
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(logDurationCallInterceptor).addPathPatterns("/**");
	}
}