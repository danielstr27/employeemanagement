package com.theWorkers.EmployeeManagement.controllers;

import static com.theWorkers.EmployeeManagement.utils.Utils.parseRegular;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import com.theWorkers.EmployeeManagement.entities.person.Child;
import com.theWorkers.EmployeeManagement.exceptions.ChildNotFoundException;
import com.theWorkers.EmployeeManagement.exceptions.EmployeeManagementException;
import com.theWorkers.EmployeeManagement.services.ChildrenService;

@RestController
@RequestMapping("/children")
public class ChildrenController {

	final private ChildrenService service;

	public ChildrenController(ChildrenService service) {
		this.service = service;
	}

	@PostMapping
	public void save(@RequestBody Child child) throws EmployeeManagementException {
		service.create(child);
	}

	@GetMapping
	public List<Child> findByRegularSpec(@RequestParam(value = "firstName", required = false) String firstName,
			@RequestParam(value = "lastName", required = false) String lastName,
			@RequestParam(value = "birthDate", required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") Date birthDate,
			@RequestParam(value = "maxBirthDate", required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") Date maxBirthDate) {
		return service.findBySpec(parseRegular(firstName, lastName, birthDate, maxBirthDate, null, null));
	}

	@GetMapping("/{id}")
	public Child findById(@PathVariable Long id) throws ChildNotFoundException {
		return service.findById(id);
	}

	@PutMapping("/{id}")
	public void save(@RequestBody Child child, @PathVariable("id") Long id) throws EmployeeManagementException {
		service.update(child, id);
	}

	@DeleteMapping("/{id}")
	public void remove(@PathVariable("id") Long id) throws EmployeeManagementException {
		service.remove(id);
	}
}
