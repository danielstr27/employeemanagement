package com.theWorkers.EmployeeManagement.controllers;

import static com.theWorkers.EmployeeManagement.utils.Utils.parseRegular;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import com.theWorkers.EmployeeManagement.entities.Address;
import com.theWorkers.EmployeeManagement.entities.person.Child;
import com.theWorkers.EmployeeManagement.entities.person.Employee;
import com.theWorkers.EmployeeManagement.entities.person.Spouse;
import com.theWorkers.EmployeeManagement.exceptions.AddressNotFoundException;
import com.theWorkers.EmployeeManagement.exceptions.ChildNotFoundException;
import com.theWorkers.EmployeeManagement.exceptions.EmployeeManagementException;
import com.theWorkers.EmployeeManagement.exceptions.EmployeeNotFoundException;
import com.theWorkers.EmployeeManagement.services.EmployeesService;

@RestController
@RequestMapping("/employees")
public class EmployeesController {

	final private EmployeesService service;

	public EmployeesController(EmployeesService service) {
		this.service = service;
	}

	@PostMapping
	public void save(@RequestBody Employee employee) throws EmployeeManagementException {
		service.create(employee);
	}

	@GetMapping
	public List<Employee> findByRegularSpec(@RequestParam(value = "firstName", required = false) String firstName,
			@RequestParam(value = "lastName", required = false) String lastName,
			@RequestParam(value = "birthDate", required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") Date birthDate,
			@RequestParam(value = "maxBirthDate", required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") Date maxBirthDate,
			@RequestParam(value = "phoneNumber", required = false) String phoneNumber,
			@RequestParam(value = "email", required = false) String email) {
		return service.findBySpec(parseRegular(firstName, lastName, birthDate, maxBirthDate, phoneNumber, email));
	}

	@GetMapping("/{id}")
	public Employee findById(@PathVariable Long id) throws EmployeeNotFoundException {
		return service.findById(id);
	}

	@GetMapping("/{id}/spouse")
	public Spouse findSpouseById(@PathVariable Long id) throws EmployeeNotFoundException {
		return service.findById(id).getSpouse();
	}

	@GetMapping("/{id}/children")
	public List<Child> findChildrenById(@PathVariable Long id) throws EmployeeNotFoundException {
		return service.findById(id).getChildren();
	}

	@GetMapping("/{id}/addresses")
	public List<Address> findAddressesById(@PathVariable Long id) throws EmployeeNotFoundException {
		return service.findById(id).getAddresses();
	}

	@GetMapping("/{id}/children/{childId}")
	public Child findChildrenById(@PathVariable Long id, @PathVariable Long childId)
			throws EmployeeNotFoundException, ChildNotFoundException {
		return service.findChild(id, childId);
	}

	@GetMapping("/{id}/addresses/{addressId}")
	public Address findAddressesById(@PathVariable Long id, @PathVariable Long addressId)
			throws EmployeeNotFoundException, AddressNotFoundException {
		return service.findAddress(id, addressId);
	}

	@PutMapping("/{id}")
	public void save(@RequestBody Employee employee, @PathVariable("id") Long id) throws EmployeeManagementException {
		service.update(employee, id);
	}

	@DeleteMapping("/{id}")
	public void remove(@PathVariable("id") Long id) throws EmployeeNotFoundException {
		service.remove(id);
	}
}
