package com.theWorkers.EmployeeManagement.controllers;

import static com.theWorkers.EmployeeManagement.utils.Utils.parseAddress;

import java.util.List;

import org.springframework.web.bind.annotation.*;

import com.theWorkers.EmployeeManagement.entities.Address;
import com.theWorkers.EmployeeManagement.exceptions.AddressNotFoundException;
import com.theWorkers.EmployeeManagement.exceptions.EmployeeManagementException;
import com.theWorkers.EmployeeManagement.services.AddressesService;

@RestController
@RequestMapping("/addresses")
public class AddressesController {

	final private AddressesService service;

	public AddressesController(AddressesService service) {
		this.service = service;
	}

	@PostMapping
	public void save(@RequestBody Address address) throws EmployeeManagementException {
		service.create(address);
	}

	@GetMapping
	public List<Address> findByRegularSpec(@RequestParam(value = "country", required = false) String country,
			@RequestParam(value = "city", required = false) String city,
			@RequestParam(value = "street", required = false) String street,
			@RequestParam(value = "postalCode", required = false) Integer postalCode) {
		return service.findBySpec(parseAddress(country, city, street, postalCode));
	}

	@GetMapping("/{id}")
	public Address findById(@PathVariable Long id) throws AddressNotFoundException {
		return service.findById(id);
	}

	@PutMapping("/{id}")
	public void save(@RequestBody Address address, @PathVariable("id") Long id) throws EmployeeManagementException {
		service.update(address, id);
	}

	@DeleteMapping("/{id}")
	public void remove(@PathVariable("id") Long id) throws EmployeeManagementException {
		service.remove(id);
	}
}
