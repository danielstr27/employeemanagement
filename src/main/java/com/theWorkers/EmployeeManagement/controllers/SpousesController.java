package com.theWorkers.EmployeeManagement.controllers;

import static com.theWorkers.EmployeeManagement.utils.Utils.parseRegular;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import com.theWorkers.EmployeeManagement.entities.person.Employee;
import com.theWorkers.EmployeeManagement.entities.person.Spouse;
import com.theWorkers.EmployeeManagement.exceptions.EmployeeManagementException;
import com.theWorkers.EmployeeManagement.exceptions.SpouseNotFoundException;
import com.theWorkers.EmployeeManagement.services.SpousesService;

@RestController
@RequestMapping("/spouses")
public class SpousesController {
	final private SpousesService service;

	public SpousesController(SpousesService service) {
		this.service = service;
	}

	@PostMapping
	public void save(@RequestBody Spouse spouse) throws EmployeeManagementException {
		service.create(spouse);
	}

	@PutMapping("/{id}")
	public void save(@RequestBody Spouse spouse, @PathVariable("id") Long id) throws EmployeeManagementException {
		service.update(spouse, id);
	}

	@GetMapping
	public List<Spouse> findByRegularSpec(@RequestParam(value = "firstName", required = false) String firstName,
			@RequestParam(value = "lastName", required = false) String lastName,
			@RequestParam(value = "birthDate", required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") Date birthDate,
			@RequestParam(value = "maxBirthDate", required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") Date maxBirthDate) {
		return service.findBySpec(parseRegular(firstName, lastName, birthDate, maxBirthDate, null, null));
	}

	@GetMapping("/{id}")
	public Spouse findById(@PathVariable Long id) throws SpouseNotFoundException {
		return service.findById(id);
	}

	@GetMapping("/{id}/spouse")
	public Employee findSpouseById(@PathVariable Long id) throws SpouseNotFoundException {
		return service.findById(id).getSpouse();
	}

	@DeleteMapping("/{id}")
	public void remove(@PathVariable("id") Long id) throws SpouseNotFoundException {
		service.remove(id);
	}
}
