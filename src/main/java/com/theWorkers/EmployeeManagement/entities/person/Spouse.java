package com.theWorkers.EmployeeManagement.entities.person;

import static java.lang.String.format;

import javax.persistence.*;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Table(name = "spouses")
public class Spouse extends Person {
	@Id
	private Long id;

	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH })
	@MapsId
	@JoinColumn(name = "spouse_id")
	@PrimaryKeyJoinColumn
	private Employee spouse;

	@Override
	public String toString() {
		return format("[id:%s, fullName:%s %s]", id, firstName, lastName);
	}
}
