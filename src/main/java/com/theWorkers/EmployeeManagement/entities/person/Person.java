package com.theWorkers.EmployeeManagement.entities.person;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
@MappedSuperclass
public abstract class Person {
	@Column(name = "first_name", nullable = false)
	protected String firstName;
	@Column(name = "last_name", nullable = false)
	protected String lastName;
	@JsonFormat(shape = JsonFormat.Shape.STRING, timezone = "Asia/Jerusalem", pattern = "dd-MM-yyyy")
	@Column(name = "birth_date", nullable = false)
	protected Date birthDate;
}
