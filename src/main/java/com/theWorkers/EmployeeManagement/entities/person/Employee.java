package com.theWorkers.EmployeeManagement.entities.person;

import static java.lang.String.format;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.theWorkers.EmployeeManagement.entities.Address;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
@Table(name = "employees")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Employee extends Person {
	@Id
	@SequenceGenerator(name = "employee_sequence", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "employee_sequence")
	private Long id;

	@Column(name = "phone_number", nullable = false)
	private String phoneNumber;

	@Column(nullable = false)
	private String email;

	@OneToOne(mappedBy = "spouse", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
	private Spouse spouse;

	@OneToMany(mappedBy = "tenant", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<Address> addresses;

	@OneToMany(mappedBy = "parent", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	private List<Child> children = new ArrayList<>();

	@Override
	public String toString() {
		return format("[id:%s, fullName:%s %s]", id, firstName, lastName);
	}
}