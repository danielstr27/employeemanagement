package com.theWorkers.EmployeeManagement.entities.person;

import static java.lang.String.format;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Table(name = "children")
@Data
@EqualsAndHashCode(callSuper = true)
@JsonIdentityInfo(scope = Child.class, generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
public class Child extends Person {

	@Id
	@SequenceGenerator(name = "child_sequence", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "child_sequence")
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "parent_id", foreignKey = @ForeignKey(name = "fk_child_employee"), nullable = false)
	private Employee parent;

	@Override
	public String toString() {
		return format("[id:%s, fullName:%s %s]", id, firstName, lastName);
	}
}
