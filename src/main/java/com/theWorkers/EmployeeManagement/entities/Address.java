package com.theWorkers.EmployeeManagement.entities;

import static java.lang.String.format;

import javax.persistence.*;

import com.theWorkers.EmployeeManagement.entities.person.Employee;

import lombok.Data;

@Entity
@Table(name = "addresses")
@Data
public class Address {
	@Id
	@SequenceGenerator(name = "address_sequence", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "address_sequence")
	private Long id;

	private String country;
	private String city;
	private String street;
	private String houseNumber;
	private int apartment;
	private int postalCode;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "tenant_id", foreignKey = @ForeignKey(name = "fk_address_employee"), nullable = false)
	private Employee tenant;

	@Override
	public String toString() {
		return format("[id:%s]", id);
	}
}
