package com.theWorkers.EmployeeManagement.utils;

import java.util.Date;

import org.springframework.data.jpa.domain.Specification;

import com.theWorkers.EmployeeManagement.entities.Address;
import com.theWorkers.EmployeeManagement.entities.person.Person;
import com.theWorkers.EmployeeManagement.queries.builders.EmployeeSpecificationsBuilder;

public class Utils {

	private static final String EQUAL_OPERATION = ":";

	public static <T extends Person> Specification<T> parseRegular(String firstName, String lastName, Date birthDate,
			Date maxBirthDate, String phoneNumber, String email) {
		EmployeeSpecificationsBuilder<T> builder = new EmployeeSpecificationsBuilder<>();
		if (firstName != null)
			builder.withString("firstName", EQUAL_OPERATION, firstName);
		if (lastName != null)
			builder.withString("lastName", EQUAL_OPERATION, lastName);
		if (maxBirthDate != null && birthDate != null) {
			builder.withDate("birthDate", "<", maxBirthDate);
			builder.withDate("birthDate", ">", birthDate);
		} else if (birthDate != null)
			builder.withDate("birthDate", EQUAL_OPERATION, birthDate);
		if (phoneNumber != null)
			builder.withString("phoneNumber", EQUAL_OPERATION, phoneNumber);
		if (email != null)
			builder.withString("email", EQUAL_OPERATION, email);

		return builder.build();
	}

	public static Specification<Address> parseAddress(String country, String city, String street, Integer postalCode) {
		EmployeeSpecificationsBuilder<Address> builder = new EmployeeSpecificationsBuilder<>();
		if (country != null)
			builder.withString("country", EQUAL_OPERATION, country);
		if (city != null)
			builder.withString("city", EQUAL_OPERATION, city);
		if (street != null)
			builder.withString("street", EQUAL_OPERATION, street);
		if (postalCode != null)
			builder.withInteger("postalCode", EQUAL_OPERATION, postalCode);

		return builder.build();
	}
}
