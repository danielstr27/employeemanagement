package com.theWorkers.EmployeeManagement.exceptions;

public class SpouseNotFoundException extends EmployeeManagementException {
	public SpouseNotFoundException() {
		super();
	}

	public SpouseNotFoundException(String message) {
		super(message);
	}

	public SpouseNotFoundException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
