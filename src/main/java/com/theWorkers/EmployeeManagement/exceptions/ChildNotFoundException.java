package com.theWorkers.EmployeeManagement.exceptions;

public class ChildNotFoundException extends EmployeeManagementException {
	public ChildNotFoundException() {
		super();
	}

	public ChildNotFoundException(String message) {
		super(message);
	}

	public ChildNotFoundException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
