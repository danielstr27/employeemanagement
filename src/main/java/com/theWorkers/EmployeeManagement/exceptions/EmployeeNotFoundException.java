package com.theWorkers.EmployeeManagement.exceptions;

public class EmployeeNotFoundException extends EmployeeManagementException {
	public EmployeeNotFoundException() {
		super();
	}

	public EmployeeNotFoundException(String message) {
		super(message);
	}

	public EmployeeNotFoundException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
