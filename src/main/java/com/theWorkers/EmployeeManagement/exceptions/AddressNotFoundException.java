package com.theWorkers.EmployeeManagement.exceptions;

public class AddressNotFoundException extends EmployeeManagementException {
	public AddressNotFoundException() {
		super();
	}

	public AddressNotFoundException(String message) {
		super(message);
	}

	public AddressNotFoundException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
