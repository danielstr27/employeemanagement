package com.theWorkers.EmployeeManagement.exceptions;

public class EmployeeManagementException extends RuntimeException {
	public EmployeeManagementException() {
		super();
	}

	public EmployeeManagementException(String message) {
		super(message);
	}

	public EmployeeManagementException(String message, Throwable throwable) {
		super(message, throwable);
	}
}
