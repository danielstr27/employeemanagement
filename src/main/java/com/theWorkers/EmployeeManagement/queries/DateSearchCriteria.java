package com.theWorkers.EmployeeManagement.queries;

import java.util.Date;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
public class DateSearchCriteria extends SearchCriteria<Date> {

	public DateSearchCriteria(String key, String operation, Date value) {
		super(key, operation, value);
	}
}