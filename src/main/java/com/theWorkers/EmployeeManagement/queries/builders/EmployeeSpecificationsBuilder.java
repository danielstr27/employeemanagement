package com.theWorkers.EmployeeManagement.queries.builders;

import static org.springframework.data.jpa.domain.Specification.where;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.jpa.domain.Specification;

import com.theWorkers.EmployeeManagement.queries.DateSearchCriteria;
import com.theWorkers.EmployeeManagement.queries.SearchCriteria;
import com.theWorkers.EmployeeManagement.queries.StringSearchCriteria;
import com.theWorkers.EmployeeManagement.queries.specifications.EmployeeManagementDateSpecification;
import com.theWorkers.EmployeeManagement.queries.specifications.EmployeeManagementIntegerSpecification;
import com.theWorkers.EmployeeManagement.queries.specifications.EmployeeManagementSpecification;
import com.theWorkers.EmployeeManagement.queries.specifications.EmployeeManagementStringSpecification;

public class EmployeeSpecificationsBuilder<T> {

	private final List<SearchCriteria> parameters;

	public EmployeeSpecificationsBuilder() {
		parameters = new ArrayList<>();
	}

	public void withString(String key, String operation, String value) {
		parameters.add(new StringSearchCriteria(key, operation, value));
	}

	public void withDate(String key, String operation, Date value) {
		parameters.add(new DateSearchCriteria(key, operation, value));
	}

	public void withInteger(String key, String operation, Integer value) {
		parameters.add(new IntegerSearchCriteria(key, operation, value));
	}

	public Specification<T> build() {
		if (parameters.size() == 0) {
			return null;
		}

		List<EmployeeManagementSpecification<T>> specs = parameters.stream().map(parameter -> {
			if (parameter instanceof DateSearchCriteria)
				return new EmployeeManagementDateSpecification<T>((DateSearchCriteria) parameter);
			else if (parameter instanceof IntegerSearchCriteria)
				return new EmployeeManagementIntegerSpecification<T>((IntegerSearchCriteria) parameter);
			return new EmployeeManagementStringSpecification<T>((StringSearchCriteria) parameter);
		}).collect(Collectors.toList());

		Specification<T> result = specs.get(0);

		for (int index = 1; index < parameters.size(); index++) {
			// decided to ignore 'or' because its less sufficient usage
			result = (where(result).and(specs.get(index)));
		}
		return result;
	}
}