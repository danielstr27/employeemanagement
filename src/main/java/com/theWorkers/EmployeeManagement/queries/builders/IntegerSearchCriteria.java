package com.theWorkers.EmployeeManagement.queries.builders;

import com.theWorkers.EmployeeManagement.queries.SearchCriteria;

public class IntegerSearchCriteria extends SearchCriteria<Integer> {
	public IntegerSearchCriteria(String key, String operation, Integer value) {
		super(key, operation, value);
	}
}
