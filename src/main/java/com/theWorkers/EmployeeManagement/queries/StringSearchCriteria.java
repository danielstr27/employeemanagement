package com.theWorkers.EmployeeManagement.queries;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
public class StringSearchCriteria extends SearchCriteria<String> {

	public StringSearchCriteria(String key, String operation, String value) {
		super(key, operation, value);
	}
}