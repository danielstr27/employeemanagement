package com.theWorkers.EmployeeManagement.queries.specifications;

import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.theWorkers.EmployeeManagement.queries.DateSearchCriteria;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class EmployeeManagementDateSpecification<T> extends EmployeeManagementSpecification<T> {

	private DateSearchCriteria dateCriteria;

	@Override
	public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		if (dateCriteria.getOperation().equalsIgnoreCase(">")) {
			return criteriaBuilder.greaterThanOrEqualTo(root.<Date>get(dateCriteria.getKey()), dateCriteria.getValue());
		} else if (dateCriteria.getOperation().equalsIgnoreCase("<")) {
			return criteriaBuilder.lessThanOrEqualTo(root.<Date>get(dateCriteria.getKey()), dateCriteria.getValue());
		} else if (dateCriteria.getOperation().equalsIgnoreCase(":")) {
			if (root.get(dateCriteria.getKey()).getJavaType() == String.class) {
				return criteriaBuilder.like(root.<String>get(dateCriteria.getKey()),
						"%" + dateCriteria.getValue() + "%");
			} else {
				return criteriaBuilder.equal(root.get(dateCriteria.getKey()), dateCriteria.getValue());
			}
		}
		return null;
	}
}
