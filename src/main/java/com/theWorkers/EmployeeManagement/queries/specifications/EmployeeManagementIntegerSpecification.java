package com.theWorkers.EmployeeManagement.queries.specifications;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.theWorkers.EmployeeManagement.queries.builders.IntegerSearchCriteria;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class EmployeeManagementIntegerSpecification<T> extends EmployeeManagementSpecification<T> {

	private IntegerSearchCriteria integerSearchCriteria;

	public Predicate toPredicate(Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
		if (integerSearchCriteria.getOperation().equalsIgnoreCase(">")) {
			return criteriaBuilder.greaterThanOrEqualTo(root.<Integer>get(integerSearchCriteria.getKey()),
					integerSearchCriteria.getValue());
		} else if (integerSearchCriteria.getOperation().equalsIgnoreCase("<")) {
			return criteriaBuilder.lessThanOrEqualTo(root.<Integer>get(integerSearchCriteria.getKey()),
					integerSearchCriteria.getValue());
		} else if (integerSearchCriteria.getOperation().equalsIgnoreCase(":")) {
			if (root.get(integerSearchCriteria.getKey()).getJavaType() == String.class) {
				return criteriaBuilder.like(root.<String>get(integerSearchCriteria.getKey()),
						"%" + integerSearchCriteria.getValue() + "%");
			} else {
				return criteriaBuilder.equal(root.get(integerSearchCriteria.getKey()),
						integerSearchCriteria.getValue());
			}
		}
		return null;
	}
}
