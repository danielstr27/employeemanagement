package com.theWorkers.EmployeeManagement.errorHandling;

import org.springframework.http.HttpStatus;

import lombok.Data;

@Data
public class EmployeeManagementError {
	private final String application_name = "EmployeeManagement";
	private HttpStatus error_code;
	private String message;

	public EmployeeManagementError(HttpStatus status, String message) {
		this.error_code = status;
		this.message = message;
	}
}
