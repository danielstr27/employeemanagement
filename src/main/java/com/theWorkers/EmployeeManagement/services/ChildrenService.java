package com.theWorkers.EmployeeManagement.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.theWorkers.EmployeeManagement.entities.person.Child;
import com.theWorkers.EmployeeManagement.exceptions.ChildNotFoundException;
import com.theWorkers.EmployeeManagement.exceptions.EmployeeManagementException;
import com.theWorkers.EmployeeManagement.exceptions.SpouseNotFoundException;
import com.theWorkers.EmployeeManagement.repositories.ChildrenRepository;

@Service
public class ChildrenService {

	private final ChildrenRepository repository;

	public ChildrenService(ChildrenRepository repository) {
		this.repository = repository;
	}

	public void create(Child child) throws EmployeeManagementException {
		if (!Optional.ofNullable(child).isPresent()) {
			throw new EmployeeManagementException("No data was delivered");
		}
		if (child.getId() != null) {
			throw new EmployeeManagementException("Invalid data was delivered");
		}

		repository.save(child);
	}

	public Child findById(Long id) throws ChildNotFoundException {
		return repository.findById(id).orElseThrow(() -> new ChildNotFoundException("No child found with such ID"));
	}

	public void remove(Long childId) throws EmployeeManagementException {
		if (childId == null) {
			throw new EmployeeManagementException("Invalid child ID");
		}
		repository.deleteById(childId);
	}

	public List<Child> findBySpec(Specification<Child> specification) {
		return repository.findAll(specification);
	}

	public void update(Child child, Long id) throws EmployeeManagementException {
		if (child == null || id == null) {
			throw new EmployeeManagementException("Invalid data was delivered");
		} else if (child.getId() != null && !child.getId().equals(id)) {
			throw new EmployeeManagementException("Invalid data was delivered");
		}

		if (findById(id) == null) {
			throw new ChildNotFoundException("No child found with such ID");
		}

		child.setId(id);

		if (child.getParent() == null) {
			child.setParent(findById(id).getParent());
		}

		repository.save(child);
	}
}
