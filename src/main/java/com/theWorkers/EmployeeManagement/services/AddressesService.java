package com.theWorkers.EmployeeManagement.services;

import java.util.List;
import java.util.Optional;

import com.theWorkers.EmployeeManagement.exceptions.SpouseNotFoundException;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.theWorkers.EmployeeManagement.entities.Address;
import com.theWorkers.EmployeeManagement.exceptions.AddressNotFoundException;
import com.theWorkers.EmployeeManagement.exceptions.EmployeeManagementException;
import com.theWorkers.EmployeeManagement.repositories.AddressesRepository;

@Service
public class AddressesService {

	private final AddressesRepository repository;

	public AddressesService(AddressesRepository repository) {
		this.repository = repository;
	}

	public void create(Address address) throws EmployeeManagementException {
		if (!Optional.ofNullable(address).isPresent()) {
			throw new EmployeeManagementException("No data was delivered");
		}
		if (address.getId() != null) {
			throw new EmployeeManagementException("Invalid data was delivered");
		}

		repository.save(address);
	}

	public Address findById(Long id) throws AddressNotFoundException {
		return repository.findById(id).orElseThrow(() -> new AddressNotFoundException("No address found with such ID"));
	}

	public void remove(Long addressId) throws EmployeeManagementException {
		if (addressId == null) {
			throw new EmployeeManagementException("Invalid address ID");
		}
		repository.deleteById(addressId);
	}

	public List<Address> findBySpec(Specification<Address> specification) {
		return repository.findAll(specification);
	}

	public void update(Address address, Long id) throws EmployeeManagementException {
		if (address == null || id == null) {
			throw new EmployeeManagementException("Invalid data was delivered");
		} else if (address.getId() != null && !address.getId().equals(id)) {
			throw new EmployeeManagementException("Invalid data was delivered");
		}

		if (findById(id) == null) {
			throw new AddressNotFoundException("No address found with such ID");
		}

		address.setId(id);

		if (address.getTenant() == null) {
			address.setTenant(findById(id).getTenant());
		}

		repository.save(address);
	}
}
