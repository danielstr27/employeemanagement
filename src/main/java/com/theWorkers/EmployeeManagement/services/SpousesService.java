package com.theWorkers.EmployeeManagement.services;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.theWorkers.EmployeeManagement.entities.person.Employee;
import com.theWorkers.EmployeeManagement.entities.person.Spouse;
import com.theWorkers.EmployeeManagement.exceptions.EmployeeManagementException;
import com.theWorkers.EmployeeManagement.exceptions.EmployeeNotFoundException;
import com.theWorkers.EmployeeManagement.exceptions.SpouseNotFoundException;
import com.theWorkers.EmployeeManagement.repositories.SpousesRepository;

@Service
public class SpousesService {

	private final SpousesRepository repository;
	private final EmployeesService employeesService;

	public SpousesService(SpousesRepository repository, EmployeesService employeesService) {
		this.repository = repository;
		this.employeesService = employeesService;
	}

	public void create(Spouse spouse) throws EmployeeManagementException {
		if (spouse == null) {
			throw new EmployeeManagementException("No data was delivered");
		}

		// isEmployeeDataMissing
		if (spouse.getSpouse() == null || spouse.getSpouse().getId() == null) {
			throw new EmployeeNotFoundException("Insufficient data was delivered");
		} else {
			// validation for employee validity and retrieval
			spouse.setId(employeesService.findById(spouse.getSpouse().getId()).getId());
			repository.save(spouse);
		}
	}

	public Spouse findById(Long spouseId) throws SpouseNotFoundException {
		return repository.findById(spouseId).orElseThrow(() -> new SpouseNotFoundException("No spouse found with such ID"));
	}

	public void remove(Long spouseId) throws SpouseNotFoundException {
		if (spouseId == null) {
			throw new SpouseNotFoundException("Invalid spouse ID");
		}

		Spouse spouse = findById(spouseId);
		Employee employee = spouse.getSpouse();
		employee.setSpouse(null);
		repository.deleteById(spouseId);
	}

	public List<Spouse> findBySpec(Specification<Spouse> specification) {
		return repository.findAll(specification);
	}

	public void update(Spouse spouse, Long id) throws EmployeeManagementException {
		if (spouse == null || (spouse.getId() == null && id == null)) {
			throw new EmployeeManagementException("Invalid data was delivered");
		} else if (spouse.getId() != null && !spouse.getId().equals(id)) {
			throw new EmployeeManagementException("Invalid data was delivered");
		}

		if (findById(id) == null) {
			throw new SpouseNotFoundException("No spouse found with such ID");
		}

		repository.save(spouse);
	}
}
