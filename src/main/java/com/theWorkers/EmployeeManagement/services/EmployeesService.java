package com.theWorkers.EmployeeManagement.services;

import static java.lang.String.format;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.theWorkers.EmployeeManagement.entities.Address;
import com.theWorkers.EmployeeManagement.entities.person.Child;
import com.theWorkers.EmployeeManagement.entities.person.Employee;
import com.theWorkers.EmployeeManagement.exceptions.AddressNotFoundException;
import com.theWorkers.EmployeeManagement.exceptions.ChildNotFoundException;
import com.theWorkers.EmployeeManagement.exceptions.EmployeeManagementException;
import com.theWorkers.EmployeeManagement.exceptions.EmployeeNotFoundException;
import com.theWorkers.EmployeeManagement.repositories.EmployeesRepository;

@Service
public class EmployeesService {
	private final EmployeesRepository repository;

	public EmployeesService(EmployeesRepository repository) {
		this.repository = repository;
	}

	public void create(Employee employee) throws EmployeeManagementException {
		if (!Optional.ofNullable(employee).isPresent()) {
			throw new EmployeeManagementException("No data was delivered");
		}
		if (employee.getId() != null) {
			throw new EmployeeManagementException("Invalid data was delivered");
		}

		repository.save(employee);
	}

	public Employee findById(Long employeeId) throws EmployeeNotFoundException {
		return repository.findById(employeeId)
				.orElseThrow(() -> new EmployeeNotFoundException("No employee found with such ID"));
	}

	public void remove(Long employeeId) throws EmployeeNotFoundException {
		if (employeeId == null) {
			throw new EmployeeNotFoundException("Invalid employee ID");
		}

		if (findById(employeeId) == null) {
			throw new EmployeeNotFoundException("No employee found with such ID");
		}

		repository.deleteById(employeeId);
	}

	public List<Employee> findBySpec(Specification<Employee> specification) {
		return repository.findAll(specification);
	}

	public void update(Employee employee, Long id) throws EmployeeManagementException {
		if (employee == null || id == null) {
			throw new EmployeeManagementException("Invalid data was delivered");
		} else if (employee.getId() != null && !employee.getId().equals(id)) {
			throw new EmployeeManagementException("Invalid data was delivered");
		}

		if (findById(id) == null) {
			throw new EmployeeNotFoundException("No employee found with such ID");
		}
		repository.save(employee);
	}

	public Child findChild(Long id, Long childId) throws EmployeeNotFoundException, ChildNotFoundException {
		Employee employee = findById(id);
		return employee.getChildren().stream().filter(x -> x.getId().equals(childId)).findAny().orElseThrow(
				() -> new ChildNotFoundException(format("Child with id: %s not found for %s", childId, employee)));
	}

	public Address findAddress(Long id, Long addressId) throws EmployeeNotFoundException, AddressNotFoundException {
		Employee employee = findById(id);
		return employee.getAddresses().stream().filter(x -> x.getId().equals(addressId)).findAny()
				.orElseThrow(() -> new AddressNotFoundException(
						format("Address with id: %s not found for %s", addressId, employee)));
	}
}
