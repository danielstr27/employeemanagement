package com.theWorkers.EmployeeManagement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.theWorkers.EmployeeManagement.entities.person.Spouse;

@Repository
public interface SpousesRepository extends JpaRepository<Spouse, Long>, JpaSpecificationExecutor<Spouse> {
}
