package com.theWorkers.EmployeeManagement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.theWorkers.EmployeeManagement.entities.person.Employee;

@Repository
public interface EmployeesRepository extends JpaRepository<Employee, Long>, JpaSpecificationExecutor<Employee> {

}
