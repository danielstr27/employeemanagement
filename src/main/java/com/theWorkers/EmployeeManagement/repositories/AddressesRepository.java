package com.theWorkers.EmployeeManagement.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.theWorkers.EmployeeManagement.entities.Address;

@Repository
public interface AddressesRepository extends JpaRepository<Address, Long>, JpaSpecificationExecutor<Address> {
}
