package com.theWorkers.EmployeeManagement.unit.services;

import static com.theWorkers.EmployeeManagement.builders.GeneralBuilder.buildChild;
import static com.theWorkers.EmployeeManagement.builders.GeneralBuilder.buildChildWithID;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.jpa.domain.Specification;

import com.theWorkers.EmployeeManagement.entities.person.Child;
import com.theWorkers.EmployeeManagement.exceptions.ChildNotFoundException;
import com.theWorkers.EmployeeManagement.exceptions.EmployeeManagementException;
import com.theWorkers.EmployeeManagement.repositories.ChildrenRepository;
import com.theWorkers.EmployeeManagement.services.ChildrenService;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class ChildrenServiceTest {

	@Mock
	private ChildrenRepository repository;

	@InjectMocks
	private ChildrenService service;

	@Test
	void givenChildrenAfterRemovalShouldBeDeleted() throws EmployeeManagementException {
		Child child = buildChildWithID();
		service.remove(child.getId());
		verify(repository, atLeastOnce()).deleteById(anyLong());
	}

	@Test
	void givenChildAfterUpdateShouldBeUpdated() throws EmployeeManagementException {
		Child child = buildChildWithID();
		when(repository.findById(anyLong())).thenReturn(Optional.of(child));
		service.update(child, child.getId());
		verify(repository, times(1)).save(any(Child.class));
	}

	@Test
	void givenNonExistingChildForUpdateThrowException() {
		Child child = buildChild();
		assertThrows(ChildNotFoundException.class, () -> {
			service.findById(child.getId());
		});
	}

	@Test
	void givenChildWithoutIdToBeUpdatedThenThrowException() {
		Child child = buildChild();
		assertThrows(EmployeeManagementException.class, () -> {
			service.update(child, child.getId());
		});
	}

	@Test
	void givenChildForAdditionShouldBeAdded() throws EmployeeManagementException {
		Child child = buildChild();
		service.create(child);
		verify(repository, times(1)).save(any(Child.class));
	}

	@Test
	void whenChildQueriedAndDoesNotExistExceptionThrown() {
		Child child = buildChild();
		assertThrows(ChildNotFoundException.class, () -> {
			service.findById(child.getId());
		});
	}

	@Test
	void whenChildrenQueriedThenReturned() {
		service.findBySpec(new Specification<Child>() {
			@Override
			public Predicate toPredicate(Root<Child> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				return null;
			}
		});
		verify(repository, times(1)).findAll(any(Specification.class));
	}
}
