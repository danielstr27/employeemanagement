package com.theWorkers.EmployeeManagement.unit.services;

import static com.theWorkers.EmployeeManagement.builders.GeneralBuilder.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.jpa.domain.Specification;

import com.theWorkers.EmployeeManagement.entities.person.Spouse;
import com.theWorkers.EmployeeManagement.exceptions.EmployeeManagementException;
import com.theWorkers.EmployeeManagement.exceptions.EmployeeNotFoundException;
import com.theWorkers.EmployeeManagement.exceptions.SpouseNotFoundException;
import com.theWorkers.EmployeeManagement.repositories.SpousesRepository;
import com.theWorkers.EmployeeManagement.services.EmployeesService;
import com.theWorkers.EmployeeManagement.services.SpousesService;

@ExtendWith(MockitoExtension.class)
public class SpouseServiceTest {

	@Mock
	private SpousesRepository repository;

	@Mock
	private EmployeesService employeesService;

	@InjectMocks
	private SpousesService service;

	@Test
	void givenSpouseAfterRemovalShouldBeDeleted() throws SpouseNotFoundException {
		Spouse spouse = buildSpouseWithID();
		when(repository.findById(anyLong())).thenReturn(Optional.of(spouse));
		service.remove(spouse.getId());
		verify(repository, atLeastOnce()).deleteById(anyLong());
	}

	@Test
	void givenSpouseAfterUpdateShouldBeUpdated() throws EmployeeManagementException {
		Spouse spouse = buildSpouseWithID();
		when(repository.findById(anyLong())).thenReturn(Optional.of(spouse));
		service.update(spouse, spouse.getId());
		verify(repository, times(1)).save(any(Spouse.class));
	}

	@Test
	void givenSpouseAfterAdditionShouldBeAdded() throws EmployeeManagementException {
		Spouse spouse = buildSpouseWithID();
		when(employeesService.findById(anyLong())).thenReturn(buildEmployeeWithID());
		service.create(spouse);
		verify(repository, times(1)).save(any(Spouse.class));
	}

	@Test
	void givenSpouseForCreationWithoutSpouseDataThenExceptionThrown() {
		Spouse spouse = buildSpouseWithID();
		spouse.setSpouse(null);
		assertThrows(EmployeeNotFoundException.class, () -> {
			service.create(spouse);
		});
	}

	@Test
	void givenInvalidDataForCreationThenExceptionThrown() {
		assertThrows(EmployeeManagementException.class, () -> {
			service.create(null);
		});
	}

	@Test
	void whenSpouseQueriedAndDoesNotExistThenExceptionThrown() {
		Spouse spouse = buildSpouse();
		assertThrows(SpouseNotFoundException.class, () -> {
			service.findById(spouse.getId());
		});
	}

	@Test
	void whenFindAllCalledThenRetrieve() throws EmployeeManagementException {
		service.findBySpec(new Specification<Spouse>() {
			@Override
			public Predicate toPredicate(Root<Spouse> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				return null;
			}
		});
		verify(repository, times(1)).findAll(any(Specification.class));
	}
}
