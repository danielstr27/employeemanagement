package com.theWorkers.EmployeeManagement.unit.services;

import static com.theWorkers.EmployeeManagement.builders.GeneralBuilder.buildEmployee;
import static com.theWorkers.EmployeeManagement.builders.GeneralBuilder.buildEmployeeWithID;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import java.util.Collections;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.theWorkers.EmployeeManagement.exceptions.AddressNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.jpa.domain.Specification;

import com.theWorkers.EmployeeManagement.entities.person.Employee;
import com.theWorkers.EmployeeManagement.exceptions.ChildNotFoundException;
import com.theWorkers.EmployeeManagement.exceptions.EmployeeManagementException;
import com.theWorkers.EmployeeManagement.exceptions.EmployeeNotFoundException;
import com.theWorkers.EmployeeManagement.repositories.EmployeesRepository;
import com.theWorkers.EmployeeManagement.services.EmployeesService;

@ExtendWith(MockitoExtension.class)
public class EmployeeServiceTest {

	@Mock
	private EmployeesRepository repository;

	@InjectMocks
	private EmployeesService service;

	@Test
	void givenEmployeeAfterRemovalShouldBeDeleted() throws EmployeeNotFoundException {
		Employee employee = buildEmployeeWithID();
		when(repository.findById(anyLong())).thenReturn(Optional.of(employee));
		service.remove(employee.getId());
		verify(repository, atLeastOnce()).deleteById(anyLong());
	}

	@Test
	void givenEmployeeAfterUpdateShouldBeUpdated() throws EmployeeManagementException {
		Employee employee = buildEmployeeWithID();
		when(repository.findById(anyLong())).thenReturn(Optional.of(employee));
		service.update(employee, employee.getId());
		verify(repository, times(1)).save(any(Employee.class));
	}

	@Test
	void givenEmployeeWithoutIdToBeUpdatedThenThrowException() throws EmployeeManagementException {
		Employee employee = buildEmployee();
		assertThrows(EmployeeManagementException.class, () -> {
			service.update(employee,employee.getId());
		});
	}

	@Test
	void givenEmployeeForAdditionShouldBeAdded() throws EmployeeManagementException {
		Employee employee = buildEmployee();
		service.create(employee);
		verify(repository, times(1)).save(any(Employee.class));
	}

	@Test
	void whenEmployeeQueriedAndDoesNotExistExceptionThrown() {
		Employee employee = buildEmployee();
		assertThrows(EmployeeNotFoundException.class, () -> {
			service.findById(employee.getId());
		});
	}

	@Test
	void whenEmployeesQueriedThenReturned() throws ChildNotFoundException, EmployeeNotFoundException {
		service.findBySpec(new Specification<Employee>() {
			@Override
			public Predicate toPredicate(Root<Employee> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				return null;
			}
		});
		verify(repository, times(1)).findAll(any(Specification.class));
	}

	@Test
	void whenEmployeeAddressesIsQueriedThenExceptionThrown() {
		Employee employee = buildEmployeeWithID();
		employee.setAddresses(Collections.emptyList());
		when(repository.findById(anyLong())).thenReturn(Optional.of(employee));
		assertThrows(AddressNotFoundException.class, () -> {
			service.findAddress(employee.getId(),0L);
		});
	}

	@Test
	void whenEmployeeChildrenIsQueriedThenExceptionThrown() {
		Employee employee = buildEmployeeWithID();
		employee.setChildren(Collections.emptyList());
		when(repository.findById(anyLong())).thenReturn(Optional.of(employee));
		assertThrows(ChildNotFoundException.class, () -> {
			service.findChild(employee.getId(),0L);
		});
	}
}
