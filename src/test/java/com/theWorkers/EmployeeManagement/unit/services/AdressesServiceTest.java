package com.theWorkers.EmployeeManagement.unit.services;

import static com.theWorkers.EmployeeManagement.builders.GeneralBuilder.buildAddress;
import static com.theWorkers.EmployeeManagement.builders.GeneralBuilder.buildAddressWithID;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.jpa.domain.Specification;

import com.theWorkers.EmployeeManagement.entities.Address;
import com.theWorkers.EmployeeManagement.exceptions.AddressNotFoundException;
import com.theWorkers.EmployeeManagement.exceptions.EmployeeManagementException;
import com.theWorkers.EmployeeManagement.repositories.AddressesRepository;
import com.theWorkers.EmployeeManagement.services.AddressesService;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class AdressesServiceTest {

	@Mock
	private AddressesRepository repository;

	@InjectMocks
	private AddressesService service;

	@Test
	void givenAddressesAfterRemovalShouldBeDeleted() throws EmployeeManagementException {
		Address address = buildAddressWithID();
		service.remove(address.getId());
		verify(repository, atLeastOnce()).deleteById(anyLong());
	}

	@Test
	void givenAddressAfterUpdateShouldBeUpdated() throws EmployeeManagementException {
		Address address = buildAddressWithID();
		when(repository.findById(anyLong())).thenReturn(Optional.of(address));
		service.update(address, address.getId());
		verify(repository, times(1)).save(any(Address.class));
	}

	@Test
	void givenNonExistingAddressForUpdateThrowException() {
		Address address = buildAddress();
		assertThrows(AddressNotFoundException.class, () -> {
			service.findById(address.getId());
		});
	}

	@Test
	void givenAddressWithoutIdToBeUpdatedThenThrowException() {
		Address address = buildAddress();
		assertThrows(EmployeeManagementException.class, () -> {
			service.update(address, address.getId());
		});
	}

	@Test
	void givenAddressForAdditionShouldBeAdded() throws EmployeeManagementException {
		Address address = buildAddress();
		service.create(address);
		verify(repository, times(1)).save(any(Address.class));
	}

	@Test
	void whenAddressQueriedAndDoesNotExistExceptionThrown() {
		Address address = buildAddress();
		assertThrows(AddressNotFoundException.class, () -> {
			service.findById(address.getId());
		});
	}

	@Test
	void whenAddressesQueriedThenReturned() {
		service.findBySpec(new Specification<Address>() {
			@Override
			public Predicate toPredicate(Root<Address> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				return null;
			}
		});
		verify(repository, times(1)).findAll(any(Specification.class));
	}
}
