package com.theWorkers.EmployeeManagement.builders;

import static java.util.Collections.emptyList;

import java.util.Date;

import com.theWorkers.EmployeeManagement.entities.Address;
import com.theWorkers.EmployeeManagement.entities.person.Child;
import com.theWorkers.EmployeeManagement.entities.person.Employee;
import com.theWorkers.EmployeeManagement.entities.person.Spouse;

public class GeneralBuilder {

	public static Employee buildEmployeeWithID() {
		Employee employee = buildEmployee();
		employee.setId(0L);
		return employee;
	}

	public static Employee buildEmployee() {
		Employee employee = new Employee();
		employee.setAddresses(emptyList());
		employee.setChildren(emptyList());
		Spouse spouse = new Spouse();
		spouse.setBirthDate(new Date());
		spouse.setFirstName("aa");
		spouse.setLastName("aaa");
		employee.setFirstName("a");
		employee.setLastName("a");
		employee.setBirthDate(new Date());
		employee.setPhoneNumber("");
		employee.setEmail("");
		employee.setSpouse(spouse);
		return employee;
	}

	public static Spouse buildSpouseWithID() {
		Spouse spouse = buildSpouse();
		spouse.setSpouse(buildEmployeeWithID());
		spouse.setId(spouse.getSpouse().getId());
		return spouse;
	}

	public static Spouse buildSpouse() {
		Spouse spouse = new Spouse();
		spouse.setBirthDate(new Date());
		spouse.setFirstName("aa");
		spouse.setLastName("aaa");
		return spouse;
	}

	public static Child buildChildWithID() {
		Child child = buildChild();
		child.setId(0L);
		return child;
	}

	public static Child buildChild() {
		Child child = new Child();
		child.setParent(buildEmployee());
		child.setBirthDate(new Date());
		child.setFirstName("Jimmy");
		child.setLastName("Smith");
		return child;
	}

	public static Address buildAddressWithID() {
		Address address = buildAddress();
		address.setId(0L);
		return address;
	}

	public static Address buildAddress() {
		Address address = new Address();
		address.setTenant(buildEmployee());
		address.setStreet("Rotschild");
		address.setPostalCode(1234567);
		address.setHouseNumber("6b");
		address.setCountry("Israel");
		address.setCity("Tel aviv");
		address.setApartment(1);
		return address;
	}
}
