package com.theWorkers.EmployeeManagement.matchers;

import java.util.List;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;

import com.theWorkers.EmployeeManagement.entities.person.Employee;

public class GeneralMatchers {
	public static Matcher<List<Employee>> quantity(int number) {
		return new BaseMatcher<List<Employee>>() {
			@Override
			public boolean matches(Object actual) {
				return ((List<Employee>) actual).size() == number;
			}

			@Override
			public void describeTo(Description description) {

			}
		};
	}
}
