#Employees Management System
The Employee management system exposes a rich REST API built on Spring Boot.
It has Employee entity(Main entity) and 3 subsequent entities: Spouse, Child, Address.
Which non of them has to be present in the employees' 'life'.
Although if the spouse exists, there could be only one for him(monogamous).
Full CRUD for each entity

Built via gradle, endpoints can be viewed in (http://localhost:8080/swagger-ui)
## Getting Started

### Running
For running the project simply run with gradle.
default host is localhost:8080 

### Endpoints
For example:

GET http://localhost:8080/employees

GET http://localhost:8080/employees/1

GET http://localhost:8080/spouses/1/spouse

GET http://localhost:8080/spouses/1

GET http://localhost:8080/spouses

GET http://localhost:8080/employees/1/addresses/1

GET http://localhost:8080/employees/1/children

GET http://localhost:8080/employees/1/spouse

GET http://localhost:8080/children

#### unit tests

The test are grouped per each service, because its the BL and there's 
sits most of the logic of the system.
To achieve more accurate testing we used Mockito and Junit5.
```
	@Test
	void givenEmployeeWithoutIdToBeUpdatedThenThrowException() throws EmployeeManagementException {
		Employee employee = buildEmployee();
		assertThrows(EmployeeManagementException.class, () -> {
			service.update(employee,employee.getId());
		});
	}
```

#### end to end tests

I haven't made end-to-end mvc tests, 
    because of minimalistic api, in addition  i've exported a wide covering 'test' suit in postman, you
    ./EmployeeManagementSystem.postman_collection.json

The endpoints call in postman are very convenient and i first wrote most of them, before coding.
Just to picture myself the desired API and what should be tested in the BL( services unit testing);

```
Give an example
```
## Built With
* [Official Gradle documentation](https://docs.gradle.org)
* [Spring Boot Gradle Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.4.0/gradle-plugin/reference/html/)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.4.0/reference/htmlsingle/#boot-features-developing-web-applications)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.4.0/reference/htmlsingle/#boot-features-jpa-and-spring-data)
and more...
## Versioning

I used [Bitbucket](http://bitbucket.com/) for versioning. See the [tags on this repository](https://bitbucket.org/danielstr27/employeemanagement/src/master/). 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

